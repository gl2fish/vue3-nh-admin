import { request } from "@/utils/service"

interface CenterOperate {
  id?: number | string
  name: string
  link: string
  branch: string
  type: number | string
  image?: string
  action: string
}

interface DataId {
  id: number | string
}

interface EditTabs {
  id?: number | string
  name: string
  type?: number | string
  action: string
}

export function getList(type?: number, keyword?: string) {
  if (type) {
    return request({
      url: `center/list?type=${type}&keyword=${keyword}`,
      method: "get"
    })
  }
  return request({
    url: `center/list?keyword=${keyword}`,
    method: "get"
  })
}

export function getTabs() {
  return request({
    url: `center/tabs`,
    method: "get"
  })
}

export function operate(data: CenterOperate) {
  return request({
    url: "center/operate",
    method: "post",
    data
  })
}

export function dele(data: DataId) {
  return request({
    url: "center/del",
    method: "post",
    data
  })
}

export function editTabs(data: EditTabs) {
  return request({
    url: "center/editTabs",
    method: "post",
    data
  })
}

export function delTabs(data: DataId) {
  return request({
    url: "center/delTabs",
    method: "post",
    data
  })
}
