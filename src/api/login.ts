import { request } from "@/utils/service"

interface IUserRequestData {
  username: string
  password: string
}

interface UserName {
  id: number
  oldName: string
  name: string
}

interface UserPasswd {
  id: number
  oldPasswd: string
  passwd: string
}

/** 登录，返回 token */
export function accountLogin(data: IUserRequestData) {
  return request({
    url: "users/login",
    method: "post",
    data
  })
}
/** 获取用户详情 */
export function userInfoRequest(id: string) {
  return request({
    url: `users/userInfo?id=${id}`,
    method: "get"
  })
}

export function modifyName(data: UserName) {
  return request({
    url: "users/modifyName",
    method: "post",
    data
  })
}

export function modifyPasswd(data: UserPasswd) {
  return request({
    url: "users/modifyPasswd",
    method: "post",
    data
  })
}
