class Keys {
  static sidebarStatus = "v3-admin-vite-sidebar-status-key"
  static token = "token"
  static Id = "id"
  static activeThemeName = "v3-admin-vite-active-theme-name-key"
}

export default Keys
