/** cookies 封装 */

import Keys from "@/constant/key"
import Cookies from "js-cookie"
import { Local } from "./index"

export const getSidebarStatus = () => Cookies.get(Keys.sidebarStatus)
export const setSidebarStatus = (sidebarStatus: string) => Cookies.set(Keys.sidebarStatus, sidebarStatus)

// export const getToken = () => Cookies.get(Keys.token)
// export const setToken = (token: string) => Cookies.set(Keys.token, token)
// export const removeToken = () => Cookies.remove(Keys.token)
export const getToken = () => Local.get(Keys.token)
export const setToken = (token: string) => Local.set(Keys.token, token)
export const removeToken = () => Local.remove(Keys.token)
export const getId = () => Local.get(Keys.Id)
export const setId = (id: string) => Local.set(Keys.Id, id)

export const getActiveThemeName = () => Cookies.get(Keys.activeThemeName)
export const setActiveThemeName = (themeName: string) => {
  Cookies.set(Keys.activeThemeName, themeName)
}
