import store from "@/store"
import { defineStore } from "pinia"
import { usePermissionStore } from "./permission"
import { getToken, removeToken, setToken, setId, getId } from "@/utils/cookies"
import { Local } from "@/utils"
import router, { resetRouter } from "@/router"
import { accountLogin, userInfoRequest } from "@/api/login"
import { RouteRecordRaw } from "vue-router"

interface UserInfoState {
  avatar: string
  email: string
  id: number
  name: string
}
interface IUserState {
  token: string
  roles: string[]
  userInfo: UserInfoState
  // userInfo: any{}
}

export const useUserStore = defineStore({
  id: "user",
  state: (): IUserState => {
    return {
      token: getToken() || "",
      roles: [],
      userInfo: {
        avatar: "",
        email: "",
        id: 0,
        name: ""
      }
    }
  },
  actions: {
    /** 设置角色数组 */
    setRoles(roles: string[]) {
      this.roles = roles
    },

    setUserInfo(info: UserInfoState) {
      this.userInfo = info
    },
    /** 登录 */
    login(userInfo: { username: string; password: string }) {
      return new Promise((resolve, reject) => {
        accountLogin({
          username: userInfo.username.trim(),
          password: userInfo.password
        })
          .then((res: any) => {
            setToken(res.data.token)
            setId(res.data.id)
            this.token = res.data.token
            resolve(true)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    /** 获取用户详情 */
    getInfo() {
      return new Promise((resolve, reject) => {
        userInfoRequest(getId())
          .then((res: any) => {
            // this.roles = res.data.roles
            const data = res.data
            this.setRoles(data.roles)
            delete data.roles
            this.setUserInfo(data)
            resolve(res)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    /** 切换角色 */
    async changeRoles(role: string) {
      const token = role + "-token"
      this.token = token
      setToken(token)
      await this.getInfo()
      const permissionStore = usePermissionStore()
      permissionStore.setRoutes(this.roles)
      resetRouter()
      permissionStore.dynamicRoutes.forEach((item: RouteRecordRaw) => {
        router.addRoute(item)
      })
    },
    /** 登出 */
    logout() {
      // removeToken()
      Local.clear()
      this.token = ""
      this.roles = []
      resetRouter()
    },
    /** 重置 token */
    resetToken() {
      removeToken()
      this.token = ""
      this.roles = []
    }
  }
})

/** 在 setup 外使用 */
export function useUserStoreHook() {
  return useUserStore(store)
}
